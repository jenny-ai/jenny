#!/bin/sh
#
# NAME: CONFIG ASSISTANT


echo "Installing required python packages..."

if ! which pip3 > /dev/null; then
    echo -e "pip3 not found! install it? (Y/n) \c"
    read
    if [ "$REPLY" = "n" ]; then
        exit 0
    fi
    sudo apt install python3-pip -y
fi

pip3 install google-api-python-client google-auth-httplib2 google-auth-oauthlib googlemaps googletrans2 httplib2 loguru python-mpd2 uber_rides wikipedia wikiquote

echo "Installed required python packages succesfully."


echo "Installing jenny assistant abilities and default status variables..."

if [ ! -d ./assistant ]; then
    echo "error: Assistant folder not found, did you run the script from the correct directory?"
    exit 1
fi

echo "Python Path [/home/pi/.local/lib/python3.6/site-packages]: \c"
read
if [ -z "${REPLY}" ]; then
    py_packages_path=/home/pi/.local/lib/python3.6/site-packages
else
    py_packages_path=${REPLY}
fi

if [ ! -d "${py_packages_path}/jenny" ]; then
    echo "error: Jenny SDK directory not found, install the sdk first."
    exit 1
fi
mkdir "${py_packages_path}/jenny/assistant"
if [ ! -d "${py_packages_path}/jenny/assistant" ]; then
    echo "error: Creating Jenny.Assistant SDK directory failed."
    exit 1
fi
mkdir "${py_packages_path}/jenny/db"
if [ ! -d "${py_packages_path}/jenny/db" ]; then
    echo "error: Creating Jenny.DB SDK directory failed."
    exit 1
fi

cp -r ./assistant/abilities/* "${py_packages_path}/jenny/assistant/"
cp -r ./assistant/db/* "${py_packages_path}/jenny/db/"
cp -r ./assistant/starter.py "${py_packages_path}/jenny/starter.py"
echo "Installed Jenny assistant abilities and default status variables succesfully."


echo "Installing and configuring Mopidy music server..."

wget -q -O - https://apt.mopidy.com/mopidy.gpg | apt-key add -
wget -q -O /etc/apt/sources.list.d/mopidy.list https://apt.mopidy.com/stretch.list
apt update
sudo apt install mopidy -y

echo "Installed and configured Mopidy music server succesfully."


exit 0

