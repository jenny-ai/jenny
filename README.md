<p align="center">
  <h1 align="center"> Hotel Guest Smart Assistant Box </h1>
</p>

<p align="center">
  <b>A smart assistant</b> that <i>everyone uses</i>.
</p>

---

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Description](#description)
- [Ability](#ability)

---

## Description

A smart assistant to be put in hotel rooms.

Refining the guest's experiences.
<br>

## Ability
* [x] Alarms.
* [x] City history.
* [x] Currency rates.
* [x] Giving feedback.
* [x] Google calender.
* [x] Google Maps
  * [x] Estimating the trip time between two addresses.
  * [x] Sending google maps location to the mobile app.
* [x] Mopidy Sound Server.
* [x] Morning text.
* [x] News.
* [x] Notes.
* [x] Tasks.
* [x] Queries.
  * [x] DuckDuckGo.
  * [x] Wikipedia.
* [x] Recommendations
  * [x] Places.
  * [x] Restaurants.
* [x] Reminders.
* [x] Translation.
* [x] Uber.
* [x] Weather-forecasting.
* [x] Welcome email.
* [x] Wifi password retrieval.

<br>
