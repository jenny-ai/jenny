import mpd

from jenny.status import Assistant


class MopidyClient(object):

    # https://python-mpd2.readthedocs.io/en/latest/topics/commands.html

    def __init__(self, host=None, port=6600):

        if host is None:
            with Assistant(init_network=True) as assistant:
                host = assistant.ip_address

        self.client = mpd.MPDClient(use_unicode=True)
        self.client.connect(host, port)

    def fetch_volume(self):
        return self.client.status()["volume"]

    def set_volume(self, vol):
        self.client.setvol(vol)

    def fetch_repeat(self):
        return self.client.status()["repeat"]

    def set_repeat(self, repeat=0):
        self.client.repeat(repeat)

    def state(self):
        return self.client.status()["state"]

    def next(self):
        self.client.next()

    def previous(self):
        self.client.previous()

    def pause(self):
        self.client.pause(1)

    def resume(self):
        self.client.pause(0)

    def stop(self):
        self.client.stop()

    def shuffle(self):
        self.client.shuffle()

    def jump_to_time(self, time_in_seconds=0):
        self.client.seekid(self.client.status()["songid"], time_in_seconds)

    def list_contents(self, directory="/"):
        return self.client.lsinfo(directory)

    def play(self, uri):
        self.client.clear()
        self.client.add(uri)
        self.client.play(0)

    def playlist(self):
        return self.client.playlist()

    def __del__(self):
        self.client.close()
        self.client.disconnect()
