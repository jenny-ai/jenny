from __future__ import print_function
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import json

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar.events']
creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('calendar', 'v3', credentials=creds)


def coming_events():
    """
    the function view the upcoming events in your calender
    :return: list with events
    """
    now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
    print('Getting the upcoming 10 events')
    events_result = service.events().list(calendarId='primary', timeMin=now,
                                          maxResults=10, singleEvents=True,
                                          orderBy='startTime').execute()
    events = events_result.get('items', [])
    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        print(start, event['summary'])


def add_local_event(title, start_date, end_date, place):
    """
    adding an event tou local json file
    :param title: string the title for your event
    :param start_date: string the start date of the event in format:  2015-05-28
    :param end_date: string the date when the event ends in format: 2015-05-28
    :param place: string where the event will take place
    :return: json file (local.json) contain the events we added
    """
    events_data = []
    events = {'event_title': title, 'start_date': start_date, 'end_date': end_date, 'place': place}
    if os.path.exists('local.json'):
        with open('local.json') as old:
            data = json.load(old)
            data.append(events)

        with open('local.json', 'w') as old:
            json.dump(data, old, indent=3)
    else:
        with open('local.json', 'w') as old:
            events_data.append(events)
            json.dump(events_data, old, indent=3)


def add_detailed_even(summary, location, description, start_date, end_date, time_zone="America/Los_Angeles"):
    """
    adding an deailed event to your google calender
    :param summary: string summarize the event
    :param location: string where the event will take place
    :param description: string describe the event as you want
    :param start_date: string the event start date in format:2015-05-28
    :param end_date: string the event end date in format:2015-05-28
    :param time_zone: string
    :return: added event to yur calender link
    """
    event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': {
            'dateTime': start_date,
            'timeZone': time_zone,
        },
        'end': {
            'dateTime': end_date,
            'timeZone': time_zone,
        },
        'recurrence': [
            'RRULE:FREQ=DAILY;COUNT=2'
        ],
        'attendees': [
            {'email': 'lpage@example.com'},
            {'email': 'sbrin@example.com'},
        ],
        'reminders': {
            'useDefault': False,
            'overrides': [
                {'method': 'email', 'minutes': 24 * 60},
                {'method': 'popup', 'minutes': 10},
            ],
        },
    }

    event = service.events().insert(calendarId='primary', body=event).execute()
    print('Event created: %s' % (event.get('htmlLink')))


def add_quick_event(text):
    """
    add a quick event with one sentence
    :param text: string in format: graduation project meeting on 8th of march at faculty of engineering
    :return:
    """
    created_event = service.events().quickAdd(
        calendarId='primary',
        text=text).execute()
    print(created_event['id'])


if __name__ == '__main__':
    coming_events()
    add_local_event('task submission', '2019-04-19T09:00:00-07:00', '2019-04-20T09:00:00-07:00', 'github')
    add_detailed_even('gp taskat', 'handasa ain shms', 'tasleem', '2019-04-19T09:00:00-07:00', '2019-04-21T09:00:00-07:00', time_zone="America/Los_Angeles")
    add_quick_event('sw meeting on 20th of April at faculty of engineering')