"""
Google Maps Wrapper
-------------------

"""


from googlemaps.client import Client
from googlemaps.distance_matrix import distance_matrix
from googlemaps.places import places_nearby


def retrieve_maps_key():
    return "AIzaSyD25Qc81cCn-a5FaOuZbKeR5XhvpWtq87I"


class MapsClient(object):
    """
    A class to interface with `Google Maps API`
    :param key: Optional, A special Google API key.
    :type key: string

    """

    def __init__(self, key=None):
        if key is None:
            key = retrieve_maps_key()
        self.client = Client(key=key)

    def distance_matrix(self, origins, destinations):
        """
        Returns the distance matrix from the source addresses to the destination addresses.

        :param origin: Origin address.
        :type origin: string
        :param destination: Destination Address.
        :type destination: string

        :rtype: result dict

        """

        try:
            return distance_matrix(
                client=self.client, origins=origins, destinations=destinations
            )
        except:
            raise ConnectionError(
                "Distance matrix retrieval failed, make sure the internet is working."
            )

    def estimated_trip_time_string(self, origin, destination):
        """
        Returns the estimated time to get to the destination address from the source address in `hh hours mm mins` format.

        :param origin: Origin address.
        :type origin: string
        :param destination: Destination Address.
        :type destination: string

        :rtype: string

        """

        if type(origin) is not str or type(destination) is not str:
            raise TypeError("estimated_trip_time accepts only string inputs.")

        dm = self.distance_matrix(origins=origin, destinations=destination)

        try:
            return dm["rows"][0]["elements"][0]["duration"]["text"]
        except:
            raise ValueError(
                "Distance matrix content error, make sure the key is working and the input data is correct."
            )

    def estimated_trip_time_in_seconds(self, origin, destination):
        """
        Returns the estimated time to get to the destination address from the source address in seconds.

        :param origin: Origin address.
        :type origin: string
        :param destination: Destination Address.
        :type destination: string

        :rtype: int

        """

        if type(origin) is not str or type(destination) is not str:
            raise TypeError("estimated_trip_time accepts only string inputs.")

        dm = self.distance_matrix(origins=origin, destinations=destination)

        try:
            return dm["rows"][0]["elements"][0]["duration"]["value"]
        except:
            raise ValueError(
                "Distance matrix content error, make sure the key is working and the input data is correct."
            )

    def places_nearby_by_keyword(self, keyword, location, radius=50000):
        """
        Returns the nearby places based on a search keyword and a location.

        :param keyword: Keyword for search.
        :type keyword: string
        :param location: Coordinates to search around in form `latitude,longitude`.
        :type location: string

        :rtype: result dict

        """

        try:
            return places_nearby(
                client=self.client, keyword=keyword, location=location, radius=radius
            )
        except:
            raise ConnectionError(
                "Nearby places retrieval failed, make sure the internet is working."
            )

    def first_nearby_place_coordinates(self, keyword, location, radius=50000):
        """
        Returns the nearest location coordinates based on a search keyword and a location.

        :param keyword: Heyword for search.
        :type keyword: string
        :param location: Coordinates to search around in form `latitude,longitude`.
        :type location: string

        :rtype: (latitude, longitude)

        """

        pn = self.places_nearby_by_keyword(
            keyword=keyword, location=location, radius=radius
        )

        try:
            return (
                pn["results"][0]["geometry"]["location"]["lat"],
                pn["results"][0]["geometry"]["location"]["lng"],
            )
        except:
            raise ValueError(
                "Nearby places content error, make sure the key is working and the input data is correct."
            )
