from datetime import datetime
from datetime import timedelta
import os
import json


def alarming(file):
    """
    check if its time for any alarm
    if there is it makes alarming and ask if stp the alarm removed if snooze it is time editted in te json data base
    :param file: string of the location of the json file that contain the alarms
    :return: the alarm text and option to stp alarming or snoozing
    """
    week_days = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    now = datetime.now()
    hour_now = now.hour
    min_now = now.minute
    day_now = now.weekday()
    if os.path.exists(file):
        print("exist")
        with open(file) as old:
            data = json.load(old)

    for key in data:
        hour1 = key['hour']
        minute1 = key['minute']
        day = key['day']
        if day == week_days[day_now] and (int(hour1) == int(hour_now)) and (int(minute1) == int(min_now)):
            os.system('start '+key['tone'])  # change to the alarm ringtone location
            print(key['text'])
            end = input("if you are alarmed enter stop or snooze to nap")
            if end == "stop":
                os.system('TASKKILL /F /IM vlc.exe /T')
                del key['text']
                del key['hour']
                del key['minute']
                del key['snooze']
                del key['tone']
                with open(file, 'w') as old:
                    json.dump(data, old, indent=3)
                break
            elif end == "snooze":
                os.system('TASKKILL /F /IM vlc.exe /T')
                snoozed = datetime.now()+timedelta(minutes=int(key['snooze']))
                key['hour'] = str(int(snoozed.hour))
                key['minute'] = str(int(snoozed.minute))
                with open(file, 'w') as old:
                    json.dump(data, old, indent=3)


if __name__ == '__main__':
    alarming("local.json")







