import os
import json


def add_alarm(time_alarm, day_alarm, alarm_txt, snoozing="1", tone="sound track path"):
    """
    function to alarm at certain time and save the alarm in json file can snooze for minutes and stop when awake
    :param tone: string of the alarm tone location
    :param time_alarm: string with the alarm time in format 1234
    :param alarm_txt: string with the alarm sentence
    :param snoozing: string for the snoozing minutes = 1 by dafault
    :param day_alarm: string which week day we want the alarm to be
    """
    # time_alarm = input('Set Alarm Time in format 0011: ')
    snoozing_mints = 1  # change to 5 or whatever in configuration

    if len(time_alarm) > 3:  # The time needs to be 4 numbers long with no semicolon e.g. 0730
        hour1 = time_alarm[0:2]  # separates the hour from the minutes
        minute1 = time_alarm[2:]  # separates the minutes from the hours
        day = day_alarm
        #print('Alarm set for: %s:%s' % (hour1, minute1))
        alarms_data = []
        alarms = {'text': alarm_txt, 'day': day, 'hour': hour1, 'minute': minute1, 'snoozing': snoozing, 'tone': tone}

        if os.path.exists('local.json'):
            with open('local.json') as old:
                data = json.load(old)
                data.append(alarms)

            with open('local.json', 'w') as old:
                json.dump(data, old, indent=3)

        else:
            with open("local.json", "w") as write_file:
                alarms_data.append(alarms)
                json.dump(alarms_data, write_file, indent=3)


if __name__ == '__main__':
    add_alarm("1600", "hi", "1")
