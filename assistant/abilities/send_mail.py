from jenny.client import Client
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
def send(guest_name,guest_email):
	sender_email = "jenny.hotel.assistant@gmail.com"
	receiver_email = guest_email
	password = "_assistant2019_"

	message = MIMEMultipart("alternative")
	message["Subject"] = "Welcome to Jenny -your hotel guest smart assistant box-"
	message["From"] = sender_email
	message["To"] = receiver_email

	# Create the plain-text and HTML version of your message
	text = """\
Hello {},

How are you doing?
I'm Jenny, your hotel assistant. You can find me in the room.

If you want to use me, you must activate me by saying `Welcome, Jenny` once you enter the room, I also recommend that you install the () app to be able to get a personalized experience with me.

See you in a bit!""".format(guest_name)


	# Add HTML/plain-text parts to MIMEMultipart message
	message.attach(MIMEText(text, "plain"))


	# Create secure connection with server and send email
	context = ssl.create_default_context()
	with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
		server.login(sender_email, password)
		server.sendmail(
			sender_email, receiver_email, message.as_string()
		)
if __name__ == '__main__':
	jenny=Client()
	guest=jenny.retrieve_guest_data()
	send(guest['name'].split(' ')[0],guest['mail'])