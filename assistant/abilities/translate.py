from googletrans import Translator
def translate(sentence,destLanguage,srcLanguage=None):
	"""
    translate a sentence from srcLanguage to destLanguage
        :param sentence: a string to be translated.
		:param destLanguage: a destination language, translate a sentence to.
        :param srcLanguage: (Optional) a source language, translate a sentence from.
		:return a translate string written in destLanguage.
    """
	translator = Translator()
	if srcLanguage == None:
		return translator.translate(sentence, dest=destLanguage).text
	return translator.translate(sentence, src=srcLanguage, dest=destLanguage).text


if __name__ == '__main__':
	print(translate('La vérité est ma lumière','english','french'))
	print(translate('La vérité est ma lumière','english'))