from gevent import sleep
from loguru import logger

from jenny.communication import Data, send_to_hotel
from jenny.speech import SpeechObject
from jenny.status import Requester


def hotel_sounds_playlist():

    playlist = None
    requesting = False

    with Requester() as requester:
        if not requester.requesting_hotel_sounds_playlist:
            requester.set_requesting_hotel_sounds_playlist(True)
            requester.sync()
            requesting = True
            logger.debug("Started requesting hotel sounds playlist.")

    while requesting:
        with Requester() as requester:
            if requester.requesting_hotel_sounds_playlist:
                send_to_hotel(Data.HOTELSOUNDS, "")
                sleep(0.5)
            else:
                playlist = requester.hotel_sounds_playlist
                requesting = False
                logger.debug("Recieved hotel sounds playlist: {}.".format(playlist))
    else:
        logger.debug("Hotel sounds playlist already being requested.")

    return playlist


if __name__ == "__main__":

    SpeechObject(hotel_sounds_playlist()).say()
