import hashlib
import binascii
import evernote.edam.userstore.constants as UserStoreConstants
import evernote.edam.type.ttypes as Types
import os


def add_note(title, content):
    """
    add note to evernote accout works on sandbox need to solve developer token

    """
    from evernote.api.client import EvernoteClient

    auth_token = "S=s1:U=953e0:E=171896b4fa1:C=16a31ba2178:P=1cd:A=en-devtoken:V=2:H=1015829790b317f2e043c2256ffaea1d"
    sandbox = False
    china = False

    client = EvernoteClient(token=auth_token, sandbox=sandbox, china=china)

    user_store = client.get_user_store()
    note_store = client.get_note_store()
    # List all of the notebooks in the user's account
    notebooks = note_store.listNotebooks()
    print("Found ", len(notebooks), " notebooks:")
    for notebook in notebooks:
        print("  * ", notebook.name)

    print()
    print("Creating a new note in the default notebook")
    print()

    version_ok = user_store.checkVersion(
        "Evernote EDAMTest (Python)",
        UserStoreConstants.EDAM_VERSION_MAJOR,
        UserStoreConstants.EDAM_VERSION_MINOR
    )
    print("Is my Evernote API version up to date? ", str(version_ok))
    print("")
    if not version_ok:
        exit(1)
    note = Types.Note()
    note.title = title
    note.content = '<?xml version="1.0" encoding="UTF-8"?>'
    note.content += '<!DOCTYPE en-note SYSTEM ' \
                    '"http://xml.evernote.com/pub/enml2.dtd">'
    note.content += '<en-note>'
    note.content += content
    note.content += '<br/>'
    note.content += '</en-note>'
    created_note = note_store.createNote(note)

    print("Successfully created a new note with GUID: ", created_note.guid)
    print(note.content)


if __name__ == '__main__':
    add_note('test', 'i work wehoo')