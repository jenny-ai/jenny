from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import httplib2
from oauth2client.contrib import gce

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/tasks']
creds = None
# The file token.pickle stores the user's access and refresh tokens, and is
# created automatically when the authorization flow completes for the first
# time.
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('tasks', 'v1', credentials=creds)


def view_my_tasks_lists():
    """

    :return: list of task lists
    """
    results = service.tasklists().list(maxResults=10).execute()
    items = results.get('items', [])

    if not items:
        print('No task lists found.')
    else:
        print('Task lists:')
        for item in items:
            print(item['title'])


def view_my_tasks():
    """

    :return:list of all the tasks in your account
    """
    tasks = service.tasks().list(tasklist='@default').execute()

    for task in tasks['items']:
        print(task['title'])


def add_task(title, body, date):
    """
    add task to your google tasks account
    :param title: task title
    :param body: task content
    :param date: due date in format: 2010-10-15T12:00:00.000Z
    :return: task in your google task account
    """
    task = {
        'title': title,
        'notes': body,
        'due': date
    }
    result = service.tasks().insert(tasklist='@default', body=task).execute()
    # print
    # result['id']


if __name__ == '__main__':
    view_my_tasks_lists()
    view_my_tasks()
    add_task('finish your task', 'week 4 tasks', '2019-10-15T12:00:00.000Z')

