from jenny.status import Assistant
from jenny.client import Client

def get_joke():
	with Assistant() as assistant:
		client=Client()
		joke=client.retrieve_joke_local(assistant.joke_index)
		assistant.increment_joke_index()

	return joke


if __name__=='__main__':
	print(get_joke())