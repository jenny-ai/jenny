import json, requests


def recommend_restaurants(food_type,lat,lng,recommendation_needed_no):
	"""
	 get n results of recommendation about specific food type
	 :param food_type: a string represents food type which need to get recommendations about it
	 :param lat,lng: integers represent latitude and longitude co-ordinates to get restaurant near to it
	 :param recommendation_needed_no: integer represents the number of needed returned recommendations
	 :return a list of dictionaries each dict contain name,location_address,lat and lng coordinates of the restaurant
	"""
	url = 'https://api.foursquare.com/v2/venues/explore'
	params = dict(
	  client_id='VKJJ0AKB1TLFD3X5DL5JYKOW5PGDYCIU4T13K2XKQW1PWK3C',
	  client_secret='BAAVGS2VXYQMFGLBANNCARNNFYYAHGWQUQXRFKY5VTSLSTJP',
	  v='20180323',
	  ll='{},{}'.format(lat,lng),
	  radius=10000,
	  query=food_type,
	  limit=recommendation_needed_no
	)
	resp = requests.get(url=url, params=params)
	data = json.loads(resp.text)
	results=data["response"]['groups'][0]['items']
	restaurants=[]
	for place in results:
		restaurant={}
		restaurant['name']=place['venue']['name']
		restaurant['location_address']=place['venue']['location']['address']+" "+place['venue']['location']['crossStreet']
		restaurant['lat']=place['venue']['location']['lat']
		restaurant['lng']=place['venue']['location']['lng']
		restaurants.append(restaurant)

	return restaurants

if __name__ == "__main__":
	print(recommend_restaurants("pizza",30.0977252,31.3305624,3))