import requests,json
import wikipedia

def searchOnDuckduckGenerator(searchString):
	"""
    Search for a keyword
        :param searchString: a keyword that searched for
		:yield a text contain the a text result of search provided by duckduckgo each time
    """
	request='https://api.duckduckgo.com/?q={}&format=json&t=hgsap'.format(searchString.replace(' ', '+'))
	r = requests.get(request)
	data = json.loads(r.text)
	if len(data['RelatedTopics'])==0:
		yield 'No Results'
	for i in range(len(data['RelatedTopics'])):
		if 'Text' in data['RelatedTopics'][i].keys():
			yield data['RelatedTopics'][i]['Text']

def searchOnWiki(searchString):
	"""
    Search for a keyword
        :param searchString: a keyword that searched for
		:return a text summary about it from wiki page
    """
	return wikipedia.summary(searchString)




if __name__ == '__main__':
	print("With duckduck:")
	gen=searchOnDuckduckGenerator('hell')
	for i in gen:
		print(i)
	print("With Wiki:")
	print(searchOnWiki('hell'))
