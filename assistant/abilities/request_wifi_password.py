from gevent import sleep
from loguru import logger

from jenny.communication import Data, send_to_hotel
from jenny.speech import SpeechObject
from jenny.status import Requester


def wifi_password():

    pw = None
    requesting = False

    with Requester() as requester:
        if not requester.requesting_wifi_password:
            requester.set_requesting_wifi_password(True)
            requester.sync()
            requesting = True
            logger.debug("Started requesting wifi password.")

    while requesting:
        with Requester() as requester:
            if requester.requesting_wifi_password:
                send_to_hotel(Data.HOTELWIFI, "")
                sleep(0.5)
            else:
                pw = requester.wifi_password
                requesting = False
                logger.debug("Recieved wifi password: {}.".format(pw))
    else:
        logger.debug("Wifi password already being requested.")

    return pw


if __name__ == "__main__":

    SpeechObject(wifi_password()).say()
