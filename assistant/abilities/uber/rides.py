"""
Uber Client Wrapper
-------------------

"""


from enum import Enum

from uber_rides.client import UberRidesClient
from uber_rides.session import OAuth2Credential, Session

from jenny.status import Assistant


def retrieve_user_credintials_data():
    from yaml import safe_load

    with open("uber_rider.yaml") as f:
        data = safe_load(f)
    return data


def retrieve_server_token():
    return "CiUvGqGQbY1GJSaGwZev9M7EShvp8OHJFZbqKpnG"


class UberClient(object):
    """
    A class to interface with `Uber Rides API`

    :param sandbox_mode: Optional, false if you want to make a realworld request.
    :type sandbox_mode: bool
    :param service: Optional, reqested service type.
    :type service: string
    :param destination_latitude: Optional, destination location's latitude.
    :type destination_latitude: float
    :param destination_longitude: Optional, destination location's longitude.
    :type destination_longitude: float
    :param source_latitude: Optional, pickup place's latitude.
    :type source_latitude: float
    :param source_longitude: Optional, pickup place's longitude.
    :type source_longitude: float
    :param request_id: Optional, a special request id in case you want to check the ride details.
    :type request_id: string
    :param server_token: Optional, application's server token to be used if the user didn't authorize the app.
    :type server_token: string

    """

    class Services(Enum):
        # https://www.ridesharingdriver.com/whats-the-difference-between-uberx-xl-uberplus-and-black-car/

        Scooter = "SCOOTER"
        UberPOOL = "UBERPOOL"
        UberX = "UBERX"
        UberXL = "UBERXL"
        UberSelect = "SELECT"
        UberBlack = "UBERBLACK"
        UberSUV = "SUV"

    def __init__(
        self,
        sandbox_mode=True,
        service=Services.UberX,
        destination_latitude=None,
        destination_longitude=None,
        source_latitude=None,
        source_longitude=None,
        request_id=None,
        server_token=None,
    ):

        with Assistant(init_hotel=True) as assistant:
            if assistant.is_connected_to_uber:
                session = Session(
                    oauth2credential=OAuth2Credential(
                        **retrieve_user_credintials_data()
                    )
                )
            else:
                if server_token is None:
                    server_token = retrieve_server_token()

                session = Session(server_token=server_token)

            if source_latitude is None or source_longitude is None:
                source_latitude = assistant.latitude
                source_longitude = assistant.longitude

        self.client = UberRidesClient(session, sandbox_mode=sandbox_mode)

        self.location_data = {
            "start_latitude": source_latitude,
            "start_longitude": source_longitude,
            "end_latitude": destination_latitude,
            "end_longitude": destination_longitude,
        }
        self.request_id = request_id
        self.service = service
        self.products = {}
        self.prices = {}
        self._check_available_products()

    def _check_available_products(self):
        """
        Checks available products in the country.

        """

        try:
            products_list = self.client.get_products(
                self.location_data["start_latitude"],
                self.location_data["start_longitude"],
            ).json.get("products")
        except:
            raise ConnectionError("Couldn't reach uber servers.")

        for prod in products_list:
            try:
                self.products[UberClient.Services(prod["display_name"].upper())] = prod[
                    "product_id"
                ]
            except:
                raise ValueError("Products parsing error.")

    def _check_prices(
        self, end_latitude, end_longitude, start_latitude, start_longitude
    ):
        """
        Checks prices of every uber product availble in the country based on a start and end location.

        :param end_latitude: Destination location's latitude.
        :type end_latitude: float
        :param end_longitude: Destination location's longitude.
        :type end_longitude: float
        :param start_latitude: Pickup place's latitude.
        :type start_latitude: float
        :param start_longitude: Pickup place's longitude.
        :type start_longitude: float

        """

        location_data = {
            "start_latitude": start_latitude,
            "start_longitude": start_longitude,
            "end_latitude": end_latitude,
            "end_longitude": end_longitude,
        }

        try:
            prices_list = self.client.get_price_estimates(**location_data).json.get(
                "prices"
            )
        except:
            raise ConnectionError("Couldn't reach uber servers.")

        for price in prices_list:
            try:
                self.prices[UberClient.Services(price["display_name"].upper())] = (
                    price["low_estimate"],
                    price["high_estimate"],
                    price["currency_code"],
                )
            except:
                raise ValueError("Prices parsing error.")

    def estimate_fare(
        self,
        service=None,
        destination_latitude=None,
        destination_longitude=None,
        source_latitude=None,
        source_longitude=None,
    ):
        """
        Estimate a service's fare based on a start and end location.

        :param service: Optional, reqested service type.
        :type service: string
        :param destination_latitude: Optional, destination location's latitude.
        :type destination_latitude: float
        :param destination_longitude: Optional, destination location's longitude.
        :type destination_longitude: float
        :param source_latitude: Optional, pickup place's latitude.
        :type source_latitude: float
        :param source_longitude: Optional, pickup place's longitude.
        :type source_longitude: float

        :rparam: (lowest_estimate, highest_estimate, currency)
        :rtype: tuple(float, float, string)

        """

        if service is None:
            service = self.service

        if (
            destination_latitude is not None
            and destination_longitude is not None
            and source_latitude is not None
            and source_longitude is not None
        ):
            location_data = {
                "start_latitude": source_latitude,
                "start_longitude": source_longitude,
                "end_latitude": destination_latitude,
                "end_longitude": destination_longitude,
            }
        else:
            location_data = self.location_data

        if len(self.prices.keys()) == 0:
            self._check_prices(**location_data)

        if service in self.products.keys():
            return self.prices[service]
        else:
            raise ValueError("This service isn't provided here.")

    def request_ride(
        self,
        service=None,
        destination_latitude=None,
        destination_longitude=None,
        source_latitude=None,
        source_longitude=None,
        seat_count=2,
    ):
        """
        Estimate a service's fare based on a start and end location.

        :param service: Optional, reqested service type.
        :type service: string
        :param destination_latitude: Optional, destination location's latitude.
        :type destination_latitude: float
        :param destination_longitude: Optional, destination location's longitude.
        :type destination_longitude: float
        :param source_latitude: Optional, pickup place's latitude.
        :type source_latitude: float
        :param source_longitude: Optional, pickup place's longitude.
        :type source_longitude: float

        :rparam: request_id
        :rtype: string

        """

        if service is None:
            service = self.service

        if (
            destination_latitude is not None
            and destination_longitude is not None
            and source_latitude is not None
            and source_longitude is not None
        ):
            location_data = {
                "start_latitude": source_latitude,
                "start_longitude": source_longitude,
                "end_latitude": destination_latitude,
                "end_longitude": destination_longitude,
            }
        else:
            location_data = self.location_data

        try:
            request = self.client.request_ride(
                product_id=self.products[service],
                **location_data,
                seat_count=seat_count
            ).json
        except:
            raise ConnectionError("Couldn't reach uber servers.")
        try:
            self.request_id = request.get("request_id")
        except:
            raise ValueError("Request-id parsing error.")

        return self.request_id

    def ride_details(self, request_id=None):
        """
        Retrieve details about a ride.

        :param request_id: Optional, reqested ride's request_id.
        :type request_id: string

        :rparam: {'status': '', ...}
        :rtype: dict

        """

        if request_id is None:
            request_id = self.request_id

        details = {}
        if request_id:
            try:
                details_json = self.client.get_ride_details(request_id).json
            except:
                raise ConnectionError("Couldn't reach uber servers.")

            details["status"] = details_json["status"]
            if details["status"] == "accepted" or details["status"] == "arriving":
                details["driver"] = details_json["driver"]["name"]
                details["driver_latitude"] = details_json["location"]["latitude"]
                details["driver_longitude"] = details_json["location"]["longitude"]
                details["phone_number"] = details_json["driver"]["phone_number"]
                details["sms_number"] = details_json["driver"]["sms_number"]
                details["rating"] = details_json["driver"]["rating"]
                details["vehicle"] = " ".join(
                    [details_json["vehicle"]["make"], details_json["vehicle"]["model"]]
                )
                details["license_plate"] = details_json["vehicle"]["license_plate"]
                details["eta"] = details_json["pickup"]["eta"]

            return details
        else:
            raise ValueError("Didn't find a valid request_id")

    def cancel_ride(self, request_id=None):
        """
        Cancel a ride.

        :param request_id: Optional, the soon-to-be-canceled ride's request_id.
        :type request_id: string

        """

        if request_id is None:
            request_id = self.request_id

        if request_id:
            try:
                self.client.cancel_ride(request_id).json
                return "success"
            except:
                raise ConnectionError("Couldn't reach uber servers.")
        else:
            raise ValueError("Didn't find a valid request_id")


__all__ = ["UberClient"]
