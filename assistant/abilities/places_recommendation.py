from argparse import ArgumentParser

import requests
import ujson as json

from jenny.client import Client
from jenny.speech import SpeechObject
from jenny.status import Assistant


def __retrieve_client_id():
    return "VKJJ0AKB1TLFD3X5DL5JYKOW5PGDYCIU4T13K2XKQW1PWK3C"


def __retrieve_client_secret():
    return "BAAVGS2VXYQMFGLBANNCARNNFYYAHGWQUQXRFKY5VTSLSTJP"


def places_recommendation_with_keyword(keyword, limit=5):

    assert isinstance(keyword, str), "Keyword must be a string."
    assert isinstance(limit, int) or limit is None, "Limit must be an integer value."

    if limit is None:
        limit = 5

    url = "https://api.foursquare.com/v2/venues/explore"
    with Assistant(init_hotel=True) as assistant:
        params = dict(
            client_id=__retrieve_client_id(),
            client_secret=__retrieve_client_secret(),
            v="20180323",
            ll=assistant.location,
            radius=10000,
            query=keyword,
            limit=limit,
        )
    places = requests.get(url=url, params=params)

    places = json.loads(places.text)["response"]["groups"]
    for grp in places:
        for venue in grp["items"]:
            yield venue["venue"]["id"], venue["venue"]["name"], " ".join(
                venue["venue"]["location"]["formattedAddress"]
            )


def places_recommendation(place_id=None, limit=5):

    assert isinstance(place_id, str) or place_id is None, "Place ID must be a string."
    assert isinstance(limit, int) or limit is None, "Limit must be an integer value."

    if limit is None:
        limit = 5

    if place_id is None:
        lst = Client().food_recommendations()[:limit]

        for dic in lst:
            yield dic["id"], dic["name"], dic["address"]
    else:
        dic = Client().retrieve_place_address(place_id=place_id)
        yield dic["id"], dic["name"], dic["address"]


if __name__ == "__main__":

    """
    system('python places_recommendation.py')
    system('python places_recommendation.py -kw "{}"'.format(keyword))
    system('python places_recommendation.py -kw "{}" -lim "{}"'.format(keyword, limit))
    system('python places_recommendation.py -pid "{}"'.format(place_id))
    system('python places_recommendation.py -pid "{}" -lim "{}"'.format(place_id, limit))
    """

    parser = ArgumentParser(description="Find places recommendations for the user.")
    parser.add_argument("--keyword", "-kw", type=str, help="Search keyword")
    parser.add_argument("--place-id", "-pid", type=str, help="Database place ID")
    parser.add_argument("--limit", "-lim", type=int, help="Result limit")
    args = parser.parse_args()

    try:
        if args.keyword is not None:
            for id, name, address in places_recommendation_with_keyword(
                keyword=args.keyword, limit=args.limit
            ):
                SpeechObject("{} {} {}".format(str(id), name, address)).say()
        else:
            for id, name, address in places_recommendation(
                place_id=args.place_id, limit=args.limit
            ):
                SpeechObject("{} {} {}".format(str(id), name, address)).say()
    except Exception as e:
        SpeechObject(e).say()
