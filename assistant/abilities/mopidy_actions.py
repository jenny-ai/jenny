from argparse import ArgumentParser

from jenny.assistant.mopidy.music import MopidyClient
from jenny.speech import SpeechObject


def pause():
    MopidyClient().pause()


def resume():
    MopidyClient().resume()


def stop():
    MopidyClient().stop()


def next():
    MopidyClient().next()


def previous():
    MopidyClient().previous()


def shuffle():
    MopidyClient().shuffle()


def state():
    return MopidyClient().state()


def volume():
    return MopidyClient().fetch_volume()


def set_volume(vol):
    MopidyClient().set_volume(vol=vol)


def repeat():
    return MopidyClient().fetch_repeat()


def toggle_repeat():
    mc = MopidyClient()
    if mc.fetch_repeat() == "1":
        mc.set_repeat(0)
    else:
        mc.set_repeat(1)


def jump_to(time_in_seconds):
    MopidyClient().jump_to_time(time_in_seconds=time_in_seconds)


def list_contents(directory):
    return MopidyClient().list_contents(directory=directory)


def play(directory):
    MopidyClient().play(directory)


if __name__ == "__main__":

    """
    system('python mopidy_actions.py --pause')
    system('python mopidy_actions.py --resume')
    system('python mopidy_actions.py --stop')
    system('python mopidy_actions.py --next')
    system('python mopidy_actions.py --previous')
    system('python mopidy_actions.py --shuffle')
    system('python mopidy_actions.py --state')
    system('python mopidy_actions.py --volume')
    system('python mopidy_actions.py --set-volume "{}"'.format(volume))
    system('python mopidy_actions.py --repeat')
    system('python mopidy_actions.py --toggle --repeat')
    system('python mopidy_actions.py --jump-to "{}"'.format(time_in_seconds))
    system('python mopidy_actions.py --list-contents "{}"'.format(directory_path))
    system('python mopidy_actions.py --play "{}"'.format(song_path))
    system('python mopidy_actions.py --play "{}"'.format(directory_path))
    """

    parser = ArgumentParser(description="Control Mopidy's MPD server.")
    parser.add_argument(
        "--pause", "-p", action="store_true", help="Pause the current song."
    )
    parser.add_argument(
        "--resume", "-r", action="store_true", help="Resume playing the current song."
    )
    parser.add_argument(
        "--stop", "-s", action="store_true", help="Stop the current song."
    )
    parser.add_argument(
        "--next", "-nx", action="store_true", help="Play the next song in the playlist."
    )
    parser.add_argument(
        "--previous",
        "-pv",
        action="store_true",
        help="Play the previous song in the playlist.",
    )
    parser.add_argument(
        "--shuffle", "-sh", action="store_true", help="Shuffle the current playlist."
    )
    parser.add_argument(
        "--state", "-st", action="store_true", help="Show the state of the server."
    )
    parser.add_argument(
        "--volume", "-vol", action="store_true", help="Show the volume of the server."
    )
    parser.add_argument(
        "--repeat",
        "-rep",
        action="store_true",
        help="Show the repeat status of the server.",
    )
    parser.add_argument(
        "--toggle",
        "-t",
        action="store_true",
        help="Toggle the value of the other parameter given to the program.",
    )
    parser.add_argument(
        "--set-volume", "-svol", type=int, help="Set the volume of the server."
    )
    parser.add_argument(
        "--jump-to",
        "-jt",
        type=int,
        help="Jump to some absolute point measered in seconds in the current song.",
    )
    parser.add_argument(
        "--list-contents", "-ls", type=str, help="List the contents of this directory."
    )
    parser.add_argument(
        "--play", "-pl", type=str, help="Play the given path's contents."
    )
    args = parser.parse_args()

    try:
        if args.pause:
            pause()
        elif args.resume:
            resume()
        elif args.stop:
            stop()
        elif args.next:
            next()
        elif args.previous:
            previous()
        elif args.state:
            SpeechObject(state()).say()
        elif args.volume:
            SpeechObject(volume()).say()
        elif args.repeat and args.toggle:
            toggle_repeat()
        elif args.repeat:
            SpeechObject(repeat()).say()
        elif args.set_volume is not None:
            set_volume(args.set_volume)
        elif args.jump_to is not None:
            jump_to(args.jump_to)
        elif args.list_contents is not None:
            SpeechObject(list_contents(args.list_contents)).say()
        elif args.play is not None:
            play(args.play)
    except:
        SpeechObject("Couldn't connect to the Mopidy music server").say()
