from argparse import ArgumentParser
from enum import Enum

from jenny.assistant.send_maps_url_to_app import send_maps_url_to_app
from jenny.assistant.uber.rides import UberClient
from jenny.speech import SpeechObject

sandbox_mode = True


class Action(Enum):

    CheckProducts = "CHECKPRODUCTS"
    EstimatePrice = "ESTIMATEPRICE"
    RequestRide = "REQUESTRIDE"
    CheckRideStatus = "CHECKRIDESTATUS"
    CheckDriverName = "CHECKDRIVERNAME"
    CheckDriverPhone = "CHECKDRIVERPHONE"
    SendDriverLocationToPhone = "SENDDRIVERLOCATIONTOPHONE"
    CheckDriverRating = "CHECKDRIVERRATING"
    CheckDriverCar = "CHECKDRIVERCAR"
    CheckDriverCarLicensePlate = "CHECKDRIVERCARLICENSEPLATE"
    CheckDriverETA = "CHECKDRIVERETA"
    CancelRide = "CANCELRIDE"


def check_products(source_latitude=None, source_longitude=None):
    assert (
        isinstance(source_latitude, float) or source_latitude is None
    ), "Source latitude must be a float."
    assert (
        isinstance(source_longitude, float) or source_longitude is None
    ), "Source longitude must be a float."

    client = UberClient(
        sandbox_mode=sandbox_mode,
        source_latitude=source_latitude,
        source_longitude=source_longitude,
    )

    return " ".join([prod.value for prod in client.products.keys()])


def estimate_price(
    service,
    destination_latitude,
    destination_longitude,
    source_latitude=None,
    source_longitude=None,
):
    assert isinstance(
        service, UberClient.Services
    ), "Service must be of type UberClient.Services."
    assert isinstance(
        destination_latitude, float
    ), "Destination latitude must be a float."
    assert isinstance(
        destination_longitude, float
    ), "Destination longitude must be a float."
    assert (
        isinstance(source_latitude, float) or source_latitude is None
    ), "Source latitude must be a float."
    assert (
        isinstance(source_longitude, float) or source_longitude is None
    ), "Source longitude must be a float."

    client = UberClient(
        sandbox_mode=sandbox_mode,
        service=service,
        destination_latitude=destination_latitude,
        destination_longitude=destination_longitude,
        source_latitude=source_latitude,
        source_longitude=source_longitude,
    )

    price_tuple = client.estimate_fare()

    return str(price_tuple[0]) + " to " + str(price_tuple[1]) + " " + price_tuple[2]


def request_ride(
    service,
    destination_latitude,
    destination_longitude,
    source_latitude=None,
    source_longitude=None,
):
    assert isinstance(
        service, UberClient.Services
    ), "Service must be of type UberClient.Services."
    assert isinstance(
        destination_latitude, float
    ), "Destination latitude must be a float."
    assert isinstance(
        destination_longitude, float
    ), "Destination longitude must be a float."
    assert (
        isinstance(source_latitude, float) or source_latitude is None
    ), "Source latitude must be a float."
    assert (
        isinstance(source_longitude, float) or source_longitude is None
    ), "Source longitude must be a float."

    client = UberClient(
        sandbox_mode=sandbox_mode,
        service=service,
        destination_latitude=destination_latitude,
        destination_longitude=destination_longitude,
        source_latitude=source_latitude,
        source_longitude=source_longitude,
    )

    return client.request_ride()


def check_ride_status(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    return client.ride_details()["status"]


def check_driver_name(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["driver"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def check_driver_phone(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["phone_number"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def send_driver_location_to_phone(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return send_maps_url_to_app(
            "{},{}".format(
                client.ride_details()["driver_latitude"],
                client.ride_details()["driver_longitude"],
            )
        )
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def check_driver_rating(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["rating"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def check_driver_car(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["vehicle"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def check_driver_car_license_plate(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["license_plate"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def check_driver_eta(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    try:
        return client.ride_details()["eta"]
    except KeyError:
        raise ValueError("Your ride request hasn't been accepted yet.")


def cancel_ride(request_id):
    assert isinstance(request_id, str), "Request ID must be a string."

    client = UberClient(sandbox_mode=sandbox_mode, request_id=request_id)

    return client.cancel_ride()


if __name__ == "__main__":

    """
    system('python uber_actions.py --action "{}"'.format('CHECKPRODUCTS'))
    system('python uber_actions.py --service "{}" --destination-latitude "{}" --destination-longitude "{}" --action "{}"'.format(service, destination_latitude, destination_longitude, action))
    system('python uber_actions.py --service "{}" --source-latitude "{}" --source-longitude "{}" --destination-latitude "{}" --destination-longitude "{}" --action "{}"'.format(service, source_latitude, source_longitude, destination_latitude, destination_longitude, action))
    system('python uber_actions.py --request-id "{}" --action "{}"'.format(request_id, action))
    """

    parser = ArgumentParser(description="Use uber from the comfort of your hotel room.")
    parser.add_argument(
        "--service", "-s", type=UberClient.Services, help="Uber service required"
    )
    parser.add_argument(
        "--source-latitude", "-slat", type=float, help="Pickup latitude coordinate"
    )
    parser.add_argument(
        "--source-longitude", "-slng", type=float, help="Pickup longitude coordinate"
    )
    parser.add_argument(
        "--destination-latitude",
        "-dlat",
        type=float,
        help="Destination latitude coordinate",
    )
    parser.add_argument(
        "--destination-longitude",
        "-dlng",
        type=float,
        help="Destination longitude coordinate",
    )
    parser.add_argument(
        "--request-id", "-rid", type=str, help="Request to be manipulated"
    )
    parser.add_argument(
        "--action", "-a", type=Action, required=True, help="Required action"
    )
    args = parser.parse_args()

    if args.action is Action.CheckProducts:
        SpeechObject(
            check_products(
                source_latitude=args.source_latitude,
                source_longitude=args.source_longitude,
            )
        ).say()
    elif args.action is Action.EstimatePrice:
        if (
            args.service is not None
            and args.destination_latitude is not None
            and args.destination_longitude is not None
        ):
            SpeechObject(
                estimate_price(
                    service=args.service,
                    destination_latitude=args.destination_latitude,
                    destination_longitude=args.destination_longitude,
                    source_latitude=args.source_latitude,
                    source_longitude=args.source_longitude,
                )
            ).say()
        else:
            raise ValueError(
                "Uber service type, and destination coordinates are required to estimate a price."
            )
    elif args.action is Action.RequestRide:
        if (
            args.service is not None
            and args.destination_latitude is not None
            and args.destination_longitude is not None
        ):
            request_id = request_ride(
                service=args.service,
                destination_latitude=args.destination_latitude,
                destination_longitude=args.destination_longitude,
                source_latitude=args.source_latitude,
                source_longitude=args.source_longitude,
            )
            print(request_id)
        else:
            raise ValueError(
                "Uber service type, and destination coordinates are required to request a ride."
            )
    elif args.action is Action.CheckRideStatus:
        if args.request_id is not None:
            SpeechObject(check_ride_status(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverName:
        if args.request_id is not None:
            SpeechObject(check_driver_name(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverPhone:
        if args.request_id is not None:
            SpeechObject(check_driver_phone(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.SendDriverLocationToPhone:
        if args.request_id is not None:
            SpeechObject(
                send_driver_location_to_phone(request_id=args.request_id)
            ).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverRating:
        if args.request_id is not None:
            SpeechObject(check_driver_rating(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverCar:
        if args.request_id is not None:
            SpeechObject(check_driver_car(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverCarLicensePlate:
        if args.request_id is not None:
            SpeechObject(
                check_driver_car_license_plate(request_id=args.request_id)
            ).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CheckDriverETA:
        if args.request_id is not None:
            SpeechObject(check_driver_eta(request_id=args.request_id)).say()
        else:
            raise ValueError(
                "Uber ride request_id is required to check a ride's details."
            )
    elif args.action is Action.CancelRide:
        if args.request_id is not None:
            SpeechObject(cancel_ride(request_id=args.request_id)).say()
        else:
            raise ValueError("Uber ride request_id is required to cancel a ride.")
