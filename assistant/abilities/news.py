news_countries={
"argentina" : "ar",
"australia" : "au",
"austria" : "at",
"belgium" : "be",
"brazil" : "br",
"bulgaria" : "bg",
"canada" : "ca",
"china" : "cn",
"colombia" : "co",
"cuba" : "cu",
"czechrepublic" : "cz",
"egypt" : "eg",
"france" : "fr",
"germany" : "de",
"greece" : "gr",
"hongkong" : "hk",
"hungary" : "hu",
"india" : "in",
"indonesia" : "id",
"ireland" : "ie",
"israel" : "il",
"italy" : "it",
"japan" : "jp",
"latvia" : "lv",
"lithuania" : "lt",
"malaysia" : "my",
"mexico" : "mx",
"morocco" : "ma",
"netherlands" : "nl",
"newzealand" : "nz",
"nigeria" : "ng",
"norway" : "no",
"philippines" : "ph",
"poland" : "pl",
"portugal" : "pt",
"romania" : "ro",
"russia" : "ru",
"saudiarabia" : "sa",
"serbia" : "rs",
"singapore" : "sg",
"slovakia" : "sk",
"slovenia" : "si",
"southafrica" : "za",
"southkorea" : "kr",
"sweden" : "se",
"switzerland" : "ch",
"taiwan" : "tw",
"thailand" : "th",
"turkey" : "tr",
"uae" : "ae",
"ukraine" : "ua",
"unitedkingdom" : "gb",
"unitedstates" : "us",
"venuzuela" : "ve"
}
from translate import translate
import requests,json
APIKey='98adc81926754813a407947c82af51ac'
def get_news_generator(country_name,category='general'):
	"""
	 get some news about specific country in some category
	 :param country_name: a string of country name
	 :param category: a string the default value is 'general' else you can choose one of these: 'business','entertainment','health','science','sports','technology'
	 :yield a tuple each time contains: (newstitle,newsdescription,newssourcename) all in english
	"""
	request='https://newsapi.org/v2/top-headlines?country={}&category={}&apiKey={}'.format(news_countries[country_name],category,APIKey)
	r = requests.get(request)
	data = json.loads(r.text)
	for news in data['articles']:
		yield (translate(news['title'],'english'),translate(news['description'],'english'),translate(news['source']['name'],'english'))

def get_news(country_name,category_list,no):
	"""
	 get top related news to country and to specific categories provided in category_list
	 :param country_name: a string of the country to get its related news
	 :param category_list: a list of string specify the categories of the needed news, one or more of these:'general','business','entertainment','health','science','sports','technology'
	 :param no: integer represent the number of the needed returning top news for each category
	"""
	news=[]
	for category in category_list:
		news_gen=get_news_generator(country_name,category)
		for i in range(no):
			news.append(next(news_gen))
	return news

if __name__ == '__main__':
	print(get_news('china',['general','business','entertainment'],3))



























