import requests
import json
APIKeyAccess="E71B63D33489F0C598F8C1FA65B01E0882775AD05C77EB17B47741B287D2E80C"

def change_to_EGP(curr):
	"""
    get a currency rate for {curr} w.r.t EGP, from Egyptian national bank
        :param curr: an abbreviation for currency name needed to be converted w.r.t EGP as USD,EUR...
		:return a dict contains:
								->data:the updated date for currency rates
								->buy:currency rate (buy)
								->sell:currency rate (sell)
    """
	currencyRate={}
	request="https://egyptrates.com/api/v1/prices/"+curr+"?token="+APIKeyAccess+"&lang=en&bank_id=4"
	r = requests.get(request)
	data = json.loads(r.text)
	currencyRate['date'],currencyRate['buy'],currencyRate['sell']=data['data'][0]['bank_update'],data['data'][0]['buy'],data['data'][0]['sell']
	return currencyRate

def currency_rate(src,dst):
	"""
    get a currency rate for {src} w.r.t {dst} using change_to_EGP
        :param src: kind of currency which need to know its rate w.r.t dst currency
		:param dst: which destination currency the source rate w.r.t it
		:return a dict contains:
								->data:the updated date for currency rates
								->buy:currency rate (buy)
								->sell:currency rate (sell)
    """
    src_egp=change_to_EGP(src)
    dst_egp=change_to_EGP(dst)
    src_dst={}
    src_dst['date'],src_dst['buy'],src_dst['sell'] = min(src_egp['date'],dst_egp['date']),str(round(float(src_egp['buy'])/float(dst_egp['buy']),3)),str(round(float(src_egp['sell'])/float(dst_egp['sell']),3))
    return src_dst


if __name__ == '__main__':
	print(change_to_EGP('EUR'))
	print(currency_rate('USD','EUR'))


