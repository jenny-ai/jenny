from argparse import ArgumentParser

from jenny.communication import Data, send_to_android_app
from jenny.speech import SpeechObject
from jenny.status import Assistant


def send_maps_url_to_app(place):

    assert isinstance(place, str), "Place must be a string."

    with Assistant() as assistant:
        if assistant.is_connected_to_application:
            send_to_android_app(
                Data.MAPS_URL,
                "https://www.google.com/maps/search/{}".format(place.replace(" ", "+")),
            )
        else:
            raise ConnectionError("Not connected to android application.")


if __name__ == "__main__":

    """
    system('python send_maps_url_to_app.py "{}"'.format(place_str))
    """

    parser = ArgumentParser(
        description="Send a search query url to your maps application."
    )
    parser.add_argument("place", type=str, help="Location name.")
    args = parser.parse_args()

    try:
        send_maps_url_to_app(args.place)
    except Exception as e:
        SpeechObject(e).say()
