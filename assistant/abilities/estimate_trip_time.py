from argparse import ArgumentParser

from jenny.assistant.google.maps import MapsClient
from jenny.speech import SpeechObject
from jenny.status import Assistant


def estimate_trip_time(destination, origin=None):

    assert isinstance(origin, str) or origin is None, "Origin must be a string."
    assert isinstance(destination, str), "Destination must be a string."

    if origin is None:
        with Assistant(init_hotel=True) as assistant:
            origin = assistant.location

    return MapsClient().estimated_trip_time_string(
        origin=origin, destination=destination
    )


if __name__ == "__main__":

    """
    system('python estimate_trip_time.py "{}" "{}"'.format(origin, destination))
    """

    parser = ArgumentParser(description="Estimate a trip time instantly.")
    parser.add_argument(
        "origin", type=str, help="Origin city, address, or location name."
    )
    parser.add_argument(
        "destination", type=str, help="Destination city, address, or location name."
    )
    args = parser.parse_args()

    try:
        SpeechObject(estimate_trip_time(args.origin, args.destination)).say()
    except Exception as e:
        SpeechObject(e).say()
