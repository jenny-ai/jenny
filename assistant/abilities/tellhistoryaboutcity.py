import wikipedia
from query import searchOnWiki


def search_about_city_history(city_name):
	"""
    Search for a city history at wiki
        :param city_name: a string for the name of the city
		:return a string result from wiki
    """
	return searchOnWiki(city_name+" "+"history")

if __name__ == '__main__':
	print(search_about_city_history("cairo"))