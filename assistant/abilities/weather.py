import requests,json
APIKeyAccess='13b9e6859f9a4d54b76235415191902'


def forecastWeather(city,daysno):
	"""
    forecast the weather for given {city} for {daysno} day
		:param city: a city, the location of forecasting.
		:param daysno: an int , no of forecasting days needed <=7.
		:return a dict contains:
							   ->date(date for forecast day): tuple of aweather condition in text, maxtemp and mintemp in C
    """
	weather={}
	r = requests.get('https://api.apixu.com/v1/forecast.json?key={}&q={}&days={}'.format(APIKeyAccess,city,str(daysno)))
	data = json.loads(r.text)
	for i in range(len(data['forecast']['forecastday'])):
		weather[data['forecast']['forecastday'][i]['date']]=(data['forecast']['forecastday'][i]['day']['condition']['text'],round(data['forecast']['forecastday'][i]['day']['maxtemp_c']),round(data['forecast']['forecastday'][i]['day']['mintemp_c']))
	return weather


if __name__ == '__main__':
	print(forecastWeather('Cairo',3))
