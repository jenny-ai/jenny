from gevent import sleep
from loguru import logger

from jenny.communication import Data, send_to_hotel
from jenny.speech import SpeechObject
from jenny.status import Assistant, Requester


def emergency():

    req_accepted = False
    requesting = False

    with Requester() as requester:
        if not requester.requesting_emergency:
            requester.set_requesting_emergency(True)
            requester.sync()
            requesting = True
            logger.debug("Started requesting emergency.")

    while requesting:
        with Requester() as requester:
            if requester.requesting_emergency:
                with Assistant() as assistant:
                    send_to_hotel(Data.HOTELEMERGENCY, "{}".format(assistant.id))
                sleep(0.5)
            else:
                req_accepted = True
                requesting = False
                logger.debug("Recieved emergency acknowledgement.")
    else:
        logger.debug("Emergency already being requested.")

    return req_accepted


if __name__ == "__main__":
    SpeechObject(emergency()).say()
