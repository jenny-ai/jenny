from news import get_news_generator
from quote import get_quote
from weather import forecastWeather

def morning_text_form(country_name,city_name,category_list,no):
	"""
	 form a morning text for the guest contains: date of the day,quote,weather and some news for specific categories
	 :param country_name: a string of the country to get its related news
	 :param city_name: a string of the city to get its day weather
	 :param category_list: a list of string specify the categories of the needed news, one or more of these:'general','business','entertainment','health','science','sports','technology'
	 :param no: integer represent the number of the needed returning top news for each category
	"""
	morning_text={}
	weather_dict=forecastWeather(city_name,1)
	morning_text['date']=list(weather_dict.keys())[0]
	morning_text['quote']=get_quote()
	morning_text['weather']=list(weather_dict.values())[0]
	news=[]
	for category in category_list:
		news_gen=get_news_generator(country_name,category)
		for i in range(no):
			news.append(next(news_gen))
	morning_text['news']=news
	return morning_text


if __name__ == '__main__':
	print(morning_text_form('egypt','cairo',['general','sports'],2))