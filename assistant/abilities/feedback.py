from argparse import ArgumentParser
from enum import Enum

from jenny.client import Client
from jenny.speech import SpeechObject


def generate_feedback_questions():

    lst = Client().feedback_questions()

    for dic in lst:
        yield dic["id"], dic["value"]


def store_feedback(question_id, answer):

    assert isinstance(question_id, str), "Question ID must be of type Product."
    assert isinstance(answer, str), "Answer must be a string."

    Client().store_cloud_feedback(question_id, answer)


if __name__ == "__main__":

    """
    system('python feedback.py -oq')
    system('python feedback.py -qid "{}" -a "{}"'.format(question_id, answer))
    """

    parser = ArgumentParser(description="Store the feedbacks.")
    parser.add_argument(
        "--output-questions",
        "-oq",
        action="store_true",
        help="Whether to start outputing questions.",
    )
    parser.add_argument(
        "--question-id", "-qid", type=str, help="Question ID being answered"
    )
    parser.add_argument("--answer", "-a", type=str, help="Question answer")
    args = parser.parse_args()

    try:
        if args.output_questions:
            for id, question in generate_feedback_questions():
                SpeechObject("{} {}".format(id, question)).say()
        else:
            if args.question_id and args.answer:
                store_feedback(question_id=args.question_id, answer=args.answer)
            else:
                raise ValueError(
                    "Question ID and Answer are required to store a feedback."
                )
    except Exception as e:
        SpeechObject(e).say()
