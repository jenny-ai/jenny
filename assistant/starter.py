from gevent import monkey


monkey.patch_all()


from loguru import logger


@logger.catch
def main():

    logger.remove(0)
    logger.add("debug{time}.log", enqueue=True)

    from gevent import sleep

    from jenny.protocol import reactor
    from jenny.status import Assistant

    while True:
        sleep(60)


if __name__ == "__main__":
    main()
